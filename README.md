# Customer-Management-System

---
## Application Description

This application is an customer management system is spring-boot app and was build on java 8, it handles a list of customers you can GET,DELETE,UPDATE and SAVE items in an  embedded database H2, the security is based on roles ADMIN and USER. the role ADMIN can do all the operation, and the role USER can just GET and SAVE , the authorization is made with JWT, the toke has the rights for every user and what a user can do and can not. 


---
## Start Application

The service is a Spring Boot application, you can run it from your Console or IDE instead if you prefer (the main class is Application).


### Generating the components:

##### JAR
```console
mvn clean package
```

##### Unit tests
```console
mvn clean test
```

##### Run in console:

```
java -jar target/customer-management-system-{version}.jar
```

##### Run in your IDE:

execute the main class com.customer_management_system.app.Application

## Test

You can check if your service is up through the following GET requests.

* http://localhost:8090/app/actuator/health to check the service health

## Steps to test the funtionality

* You need to create a user with the sign on end point it return http code OK
sign on Url= POST : http://localhost:8090/app/auth/sign-on
  
###Request
```
{"email":"Test01@gmail.com",
"firstName":"Test",
"password":"Test1234",
"roles":["admin"],
"username":"TestUser01"}

````
####or 

```
{"email":"Test01@gmail.com",
"firstName":"Test",
"password":"Test1234",
"roles":["user"],
"username":"TestUser01"}
```

* then you have sign in to get the token needed for every request acording to the user role there are some endpoint you may not be able to access.
sign on Url= POST: 
  http://localhost:8090/app/auth/login
####Request
```
{
  
  "userName": "TestUser01",
  "password": "Test1234"
}
```
###Response  
```
{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJSaWNoaWVaYXVsIiwiaWF0IjoxNjExNzk2NjEzLCJleHAiOjE2MTE4Mzk4MTN9.qiYejRt-rx_UhOIWvb06JdXqwL7eItS6iCSZ-ae05CLiuK4_1CKg-kU3OfGQ-TwzzNMXpPGLOYpVlCKFr5vf6g",
    "bearer": "Bearer",
    "userName": "TestUser01",
    "authorities": [
        {
            "authority": "ROLE_USER"
        },
        {
            "authority": "ROLE_ADMIN"
        }
    ]
}
```
#####or 

```

{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJSaWNoaWVaYXVsIiwiaWF0IjoxNjExNzk2NjEzLCJleHAiOjE2MTE4Mzk4MTN9.qiYejRt-rx_UhOIWvb06JdXqwL7eItS6iCSZ-ae05CLiuK4_1CKg-kU3OfGQ-TwzzNMXpPGLOYpVlCKFr5vf6g",
    "bearer": "Bearer",
    "userName": "TestUser01",
    "authorities": [
        {
            "authority": "ROLE_USER"
        }
    ]
}

```

- With this token you will be able to access the endpoint  

- The collection to test is on folder test/postman-collection-tests, 
you can find there.
  * cibp-customer-service.postman_collection.json
  * inventory-management-system-environment.postman_environment.json 

 * Import in post-man and runner them 


## List of endpoint 

* it gets all customers from database, Role{user,admin}
GET : http://localhost:{port#}/app/v1/customers


* Gets customers by ssn, Role{user,admin}
GET :  http://localhost:{port#}/app/v1/customers?ssn=852741963

  
* Get customer Page And SortBy one of the field, Role{user,admin}
GET : http://localhost:{port#}/app/v1/customers-catalog?pageSize=2&page=3&sortBy=firstName;
  

* Delete customer By ssn, Role{admin}
DELETE : http://localhost:{port#}/app/v1/customers?ssn=852741963
  

* Saved customer in database Role{admin}
  POST: http://localhost:{port#}/app/v1/customers
```
{
"firstName":"Raul",
"lastName": "Mendes",
"socialSecurity": "852147963",
"customerAddress":[
    {
"street":"Sprint",
"apartment":"B4",
"state": "NY",
"city": "NY",
"zipCode": "12345",
"country": "US"
    }
]

}
```
* update new Items in database  Role{user,admin}
http://localhost:8090/app/v1/customers?ssn=741258963&status=ACTIVE






