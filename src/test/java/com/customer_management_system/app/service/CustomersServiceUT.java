package com.customer_management_system.app.service;


import com.customer_management_system.app.entity.Customer;
import com.customer_management_system.app.exeption.BadRequestException;
import com.customer_management_system.app.exeption.RecordNotFoundException;
import com.customer_management_system.app.model.dto.CustomerDto;
import com.customer_management_system.app.repository.CustomerRepository;
import com.customer_management_system.app.testUtil.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


/**
 * ItemsService class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public class CustomersServiceUT {

    /**
     * The mock CustomerRepository.
     */
    @Mock
    private CustomerRepository customerRepository;


    /**
     * The spy CustomersService
     */
    @Spy
    @InjectMocks
    private CustomersService customersService;


    /**
     * Initialize the test mocks
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    /**
     * This is testing the correct functionality of method getAllICustomers.
     * The method is invoked and then return All Customer existing in database.
     */
    @Test
    public void WhenGetAllICustomers_IsInvoked_ThenReturnAllTheRecordInDataBase() {


        doReturn(TestUtil.getCustomerList()).when(this.customerRepository).findAll();

        final Optional<List<CustomerDto>> allCustomers = customersService.getAllCustomers();

        assertNotNull(allCustomers.get());
        assertEquals(4, allCustomers.get().size());


    }

    /**
     * This is testing the correct functionality of method getAllICustomers.
     * The method is invoked and then return All customers existing in database.
     */
    @Test
    public void WhenGetAllItems_IsInvokedAndDataBaseIsEmpty_ThenReturnRecordNotFoundException() {

        doReturn(new ArrayList<>()).when(this.customerRepository).findAll();

        assertThrows(RecordNotFoundException.class, customersService::getAllCustomers);

    }


    /**
     * This is testing the correct functionality of method addCustomers.
     * The method is invoked and save the Record in database then return save Customer database.
     */
    @Test
    public void WhenAddCustomers_IsInvokedWithValidRequest_ThenReturnRecordSavedInDataBase() throws Exception {


        doReturn(true).when(this.customerRepository).existsBySocialSecurity(any());
        doReturn(new Customer()).when(this.customerRepository).save(any());

        final Optional<CustomerDto> customerDto = customersService.addCustomers(new CustomerDto());

        assertNotNull(customerDto.get());


    }

    /**
     * This is testing the correct functionality of method AddCustomers.
     * The method is invoked with existing Record in database then return Return BadRequestException.
     */
    @Test
    public void WhenAddCustomers_IsInvokedWithExistingRecord_ThenReturnBadRequestException() throws Exception {

        doReturn(true).when(this.customerRepository).existsById(any());

        final CustomerDto customerDto = new CustomerDto();
        customerDto.setSocialSecurity("8521465475");
        assertThrows(BadRequestException.class, () -> customersService.addCustomers(customerDto));


    }


    /**
     * This is testing the correct functionality of method AddCustomers.
     * The method is invoked with Null Record in database then return IllegalArgumentException.
     */
    @Test
    public void WhenAddCustomers_IsInvokedWithNullRecord_ThenReturnIllegalArgumentException() throws Exception {

        assertThrows(IllegalArgumentException.class, () -> customersService.addCustomers(null));


    }


    /**
     * This is testing the correct functionality of method UpdateCustomers.
     * The method is invoked and save the Record in database then return save items database.
     */
    @Test
    public void WhenUpdateCustomers_IsInvokedWithValidRequest_ThenReturnRecordSavedInDataBase() throws Exception {

        Customer customer = new Customer();

        when(this.customerRepository.findBySocialSecurity(any())).thenReturn(Optional.ofNullable(customer));
        when(this.customerRepository.save(any())).thenReturn(new Customer());

        final Optional<CustomerDto> Items = customersService.updateCustomers("Active", "852741963");

        assertNotNull(Items.get());


    }

    /**
     * This is testing the correct functionality of method UpdateCustomers.
     * The method is invoked with not existing Record in database then return Return RecordNotFoundException.
     */
    @Test
    public void WhenUpdateCustomers_IsInvokedWithNoExistingRecord_ThenReturnRecordNotFoundException() throws Exception {

        when(this.customerRepository.existsBySocialSecurity(any())).thenReturn(false);

        assertThrows(RecordNotFoundException.class, () -> customersService.updateCustomers("Active", "852741963"));


    }
//
//

    /**
     * This is testing the correct functionality of method UpdateCustomers.
     * The method is invoked with Null param in database then return IllegalArgumentException.
     */
    @Test
    public void WhenUpdateCustomers_IsInvokedWithNullRecord_ThenReturnIllegalArgumentException() throws Exception {

        assertThrows(IllegalArgumentException.class, () -> customersService.updateCustomers(null, "788778785"));


    }


    /**
     * This is testing the correct functionality of method DeleteCustomerById.
     * The method is invoked and delete Record in database then return the count of deleted records.
     */
    @Test
    public void WhenDeleteCustomerById_IsInvokedWithValidRequest_ThenReturnTrue() throws Exception {

        doReturn(1).when(this.customerRepository).deleteCustomerBySocialSecurity(any());

        final Boolean deleted = customersService.deleteCustomerBySsn("852147963");

        assertTrue(deleted);


    }


    /**
     * This is testing the correct functionality of method deleteItemsById.
     * The method is invoked and delete Record in database then return the count of deleted records.
     */
    @Test
    public void WhenDeleteItemsById_IsInvokedWithNoExistingRecord_ThenReturnRecordNotFoundException() throws Exception {

        doReturn(0).when(this.customerRepository).deleteCustomerBySocialSecurity(any());

        assertThrows(RecordNotFoundException.class, () -> customersService.deleteCustomerBySsn("852147963"));


    }


    /**
     * This is testing the correct functionality of method GetCustomerBySsn.
     * The method is invoked with and Existing id and then return record from database.
     */
    @Test
    public void WhenGetCustomerBySsn_IsInvokedWithExistingRecord_ThenReturnRecordInDataBase() throws Exception {


        doReturn(Optional.ofNullable(new Customer())).when(this.customerRepository).findBySocialSecurity(any());

        final Optional<CustomerDto> Items = customersService.getCustomerBySsn("852741963");

        assertNotNull(Items.get());


    }


    /**
     * This is testing the correct functionality of method GetCustomerBySsn.
     * The method is invoked with and no existing id and then return RecordNotFoundException
     */
    @Test
    public void WhenGetCustomerBySsn_IsInvokedWithNoExistingRecord_ThenReturnRecordNotFoundException() throws Exception {


        doReturn(Optional.empty()).when(this.customerRepository).findBySocialSecurity(any());
        assertThrows(RecordNotFoundException.class, () -> customersService.getCustomerBySsn("852369741"));

    }


    /**
     * This is testing the correct functionality of method CustomerBySsn.
     * The method is invoked with and null id and then return IllegalArgumentException
     */
    @Test
    public void WhenCustomerBySsn_IsInvokedWithNullId_ThenReturnIllegalArgumentException() throws Exception {

        assertThrows(IllegalArgumentException.class, () -> customersService.getCustomerBySsn(null));

    }


}
