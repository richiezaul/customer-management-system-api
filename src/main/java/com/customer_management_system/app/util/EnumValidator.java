package com.customer_management_system.app.util;

import org.springframework.boot.SpringApplication;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * EnumValidator annotation.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 22, 2021
 * @see SpringApplication
 */
@Documented
@Constraint(
        validatedBy = {EnumValidatorImpl.class}
)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface EnumValidator {

    /**
     * enumClazz
     * @return
     */
    Class<? extends Enum<?>> enumClazz();

    /**
     * default
     * @return
     */
    String message() default "Invalid";

    /**
     * groups
     * @return
     */
    Class<?>[] groups() default {};

    /**
     * Payload
     * @return
     */
    Class<? extends Payload>[] payload() default {};
}