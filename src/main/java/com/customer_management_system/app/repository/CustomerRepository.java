package com.customer_management_system.app.repository;

import com.customer_management_system.app.entity.Customer;
import org.springframework.boot.SpringApplication;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


/**
 * item Repository.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, String> {

    /**
     * findAll
     * @return List<Customer>
     */
    @Override
    List<Customer> findAll();


    /**
     *  delete Customers By ssn
     * @param ssn
     * @return
     */
    @Transactional
    @Modifying(flushAutomatically = true)
    Integer deleteCustomerBySocialSecurity(String ssn);



    /**
     *  find By Customer Status
     * @param status
     * @return
     */
    List<Customer> findByCustomerStatus(String status);

    /**
     *  exists By Social Security
     * @param ssn
     * @return
     */
    Boolean existsBySocialSecurity(String ssn);



    /**
     *  find By Social Security
     * @param ssn
     * @return  Optional<Customer>
     */
    Optional<Customer> findBySocialSecurity(String ssn);



}
