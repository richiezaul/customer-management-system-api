package com.customer_management_system.app.repository;

import com.customer_management_system.app.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 *  User Repository
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * findByUserName
     * @param userName
     * @return Optional<User>
     */
    Optional<User> findByUserName(String userName);

    /**
     * existsByUserName
     * @param userName
     * @return Boolean
     */
    Boolean existsByUserName(String userName);

    /**
     * existsByEmail
     * @param email
     * @return Boolean
     */
    Boolean existsByEmail (String email);

}

