package com.customer_management_system.app.model;


import com.customer_management_system.app.util.EnumValidator;
import com.customer_management_system.app.model.enums.CustomerStatus;
import org.springframework.boot.SpringApplication;

/**
 * item Entity mapping.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 22, 2021
 * @see SpringApplication
 */
public class UpdateCustomerRequest extends CustomerRequest {

    /**
     * the status of the customer
     */
    @EnumValidator(enumClazz = CustomerStatus.class)
    private String customerStatus;

    /**
     * Gets customerStatus .
     *
     * @return Value of customerStatus.
     */
    public String getCustomerStatus() {
        return customerStatus;
    }

    /**
     * Sets new the customerStatus item id.
     *
     * @param customerStatus New value of the unique item id.
     */
    public void setItemId(String customerStatus) {
        this.customerStatus = customerStatus;
    }
}
