package com.customer_management_system.app.model;


import com.customer_management_system.app.entity.Address;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.boot.SpringApplication;

import java.util.List;

/**
 * ItemRequest class
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class CustomerRequest {


    @JsonIgnore
    private Integer customerId;

    /**
     * the first name of the customer
     */
    private String firstName;

    /**
     * the last name of the customer
     */
    private String lastName;

    /**
     * customer Social security
     */
    private String socialSecurity;


    /**
     * customers address
     */
    private List<Address> customerAddress;


    /**
     * Sets new the last name of the customer.
     *
     * @param lastName New value of the last name of the customer.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the first name of the customer.
     *
     * @return Value of the first name of the customer.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets customers address.
     *
     * @return Value of customers address.
     */
    public List<Address> getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Sets new customers address.
     *
     * @param customerAddress New value of customers address.
     */
    public void setCustomerAddress(List<Address> customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * Sets new customerId.
     *
     * @param customerId New value of customerId.
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the last name of the customer.
     *
     * @return Value of the last name of the customer.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets new the first name of the customer.
     *
     * @param firstName New value of the first name of the customer.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets customerId.
     *
     * @return Value of customerId.
     */
    public Integer getCustomerId() {
        return customerId;
    }


    /**
     * Gets customer Social security.
     *
     * @return Value of customer Social security.
     */
    public String getSocialSecurity() {
        return socialSecurity;
    }

    /**
     * Sets new customer Social security.
     *
     * @param socialSecurity New value of customer Social security.
     */
    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }
}