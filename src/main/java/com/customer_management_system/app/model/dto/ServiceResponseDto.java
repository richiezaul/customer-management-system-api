package com.customer_management_system.app.model.dto;

import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * ServiceResponseDto class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class ServiceResponseDto {


    /**
     * The data.
     */
    private List<? extends DataDto> data = new ArrayList<>();

    /**
     * The errors.
     */
    private List<ErrorDto> errors = new ArrayList<> ();

    /**
     * Instantiates a response dto.
     */
    public ServiceResponseDto () {
    }


    /**
     * Instantiates a new response dto.
     *
     * @param dataDtos the data dtos
     */
    public ServiceResponseDto (List<? extends DataDto> dataDtos) {
        this.data = Optional.ofNullable (dataDtos).map (ArrayList::new).orElse (null);
    }

    /**
     * Instantiates a new  response dto.
     *
     * @param error the error
     */
    public ServiceResponseDto (ErrorDto error) {
        this.errors.add (error);
    }


    /**
     * Gets the data.
     *
     * @return the data
     */
    public List<? extends DataDto> getData () {
        return Optional.ofNullable (data).map (ArrayList::new).orElse (null);
    }

    /**
     * Sets the data.
     *
     * @param data the new data
     */
    public void setData (List<? extends DataDto> data) {
        this.data = Optional.ofNullable (data).map (ArrayList::new).orElse (null);
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public List<ErrorDto> getErrors () {
        return Optional.ofNullable (errors).map (ArrayList::new).orElse (null);
    }

    /**
     * Sets the errors.
     *
     * @param errors the new errors
     */
    public void setErrors (List<ErrorDto> errors) {
        this.errors = Optional.ofNullable (errors).map (ArrayList::new).orElse (null);
    }

    /**
     * Adds the error.
     *
     * @param error the error
     */
    public void addError (ErrorDto error) {
        this.errors.add (error);
    }
}
