package com.customer_management_system.app.model.dto;

import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * CommonResponseDto enum.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class CommonResponseDto {

    /**
     * data
     */
    private List<? extends DataDto> data = new ArrayList(0);

    /**
     * errors
     */
    private List<ErrorDto> errors = new ArrayList(0);

    /**
     * Default Constructor
     */
    public CommonResponseDto() {
    }

    /**
     *  Data list Constructor
     * @param lstDataDtos
     */
    public CommonResponseDto(List<? extends DataDto> lstDataDtos) {
        List<? extends DataDto> immutableList = lstDataDtos != null ? new ArrayList(lstDataDtos) : null;
        this.data = immutableList;
    }

    /**
     * Error Constructor
     * @param error
     */
    public CommonResponseDto(ErrorDto error) {
        this.errors.add(error);
    }

    /**
     * getData
     * @return
     */
    public List<? extends DataDto> getData() {
        return Optional.ofNullable(this.data).map(ArrayList::new).orElse(null);
    }

    /**
     * setData
     * @param lstData
     */
    public void setData(List<? extends DataDto> lstData) {
        this.data = Optional.ofNullable(lstData).map(ArrayList::new).orElse(null);
    }

    /**
     * getErrors
     * @return List<ErrorDto>
     */
    public List<ErrorDto> getErrors() {
        return Optional.ofNullable(this.errors).map(ArrayList::new).orElse(null);
    }

    /**
     * setErrors
     * @param lstErrors
     */
    public void setErrors(List<ErrorDto> lstErrors) {
        this.errors = (List)Optional.ofNullable(lstErrors).map(ArrayList::new).orElse(null);
    }

    /**
     * addError
     * @param error
     */
    public void addError(ErrorDto error) {
        this.errors.add(error);
    }
}