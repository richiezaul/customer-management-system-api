package com.customer_management_system.app.exeption;

import org.springframework.boot.SpringApplication;

/**
 * BadRequestException.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class BadRequestException extends RuntimeException {

    /**
     * title
     */
    private String title;

    /**
     * Constructor with message
     * @param message
     */
    public BadRequestException(String message) {
        super(message);
    }

    /**
     * Constructor with message and title
     * @param message
     * @param title
     */
    public BadRequestException(String message, String title) {
        super(message);
        this.title = title;
    }

    /**
     * getTitle
     * @return
     */
    public String getTitle() {
        return this.title == null ? "Invalid data" : this.title;
    }

    /**
     * setTitle
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }
}