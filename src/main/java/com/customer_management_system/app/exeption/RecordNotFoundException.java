package com.customer_management_system.app.exeption;

import org.springframework.boot.SpringApplication;

import java.util.Map;


/**
 * RecordNotFoundException class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public class RecordNotFoundException extends BaseSearchException {
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * @param searchName
     * @param searchParams
     */
    public RecordNotFoundException(String searchName, Map<String, String> searchParams) {
        super.setSearchParams(searchParams);
        super.setSearchName(searchName);
    }

    /**
     * Constructor
     * @param searchName
     */
    public RecordNotFoundException(String searchName) {
        super.setSearchName(searchName);
    }
}
