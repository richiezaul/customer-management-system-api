package com.customer_management_system.app.exeption;

import org.springframework.boot.SpringApplication;

import java.util.Map;


/**
 * RecordAlreadyExistsException class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public class RecordAlreadyExistsException extends BaseSearchException {
    private static final long serialVersionUID = 2841622053274560857L;

    /**
     * RecordAlreadyExistsException Constructor
     * @param recordName
     * @param params
     */
    public RecordAlreadyExistsException(String recordName, Map<String, String> params) {
        super.setSearchName(recordName);
        super.setSearchParams(params);
    }
}