package com.customer_management_system.app.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * WebMvc Configuration.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter implements WebMvcConfigurer{


        /**
         * SWAGGER_UI
         */
        private final static String SWAGGER_UI = "swagger-ui.html";//SWAGGER_UI

        /**
         * CLASSPATH_RESOURCES
         */
        private final static String CLASSPATH_RESOURCES = "classpath:/META-INF/resources/";//CLASSPATH

        /**
         * WEBJARS
         */
        private final static String WEBJARS = "/webjars/**";//WEBJARS

        /**
         * CLASSPATH_RESOURCES_WEBJARS
         */
        private final static String CLASSPATH_RESOURCES_WEBJARS = "classpath:/META-INF/resources/webjars/";//CLASSPATH


        /**
         * WebMvcConfig method.
         *
         * @param registry the registry
         */
        @Override
        public void addResourceHandlers (ResourceHandlerRegistry registry) {

            registry.addResourceHandler (SWAGGER_UI)
                    .addResourceLocations (CLASSPATH_RESOURCES);

            registry.addResourceHandler (WEBJARS)
                    .addResourceLocations (CLASSPATH_RESOURCES_WEBJARS);

        }


    }

