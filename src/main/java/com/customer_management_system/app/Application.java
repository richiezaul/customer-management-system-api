package com.customer_management_system.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot application
 *
 * CustomerManagementSystemApplication
 */
@SpringBootApplication
public class Application {

    /**
     * main method
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
