package com.customer_management_system.app.entity;


import org.springframework.boot.SpringApplication;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * item Entity mapping.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 22, 2021
 * @see SpringApplication
 */
@Entity
@Table(name = "USERS")
public class User {

    /**
     * Unique id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private int userId;

    /**
     * user firstName
     */
    @NotNull
    @Column(name = "Company_NAME")
    private String companyName;

    /**
     * the user name
     */
    @NotNull
    @Column(name = "USER_NAME",unique = true)
    private String userName;

    /**
     * user email
     */
    @NotNull
    @Column(name = "EMAIL",unique = true)
    private String email;

    /**
     * user password
     */
    @NotNull
    @Column(name = "PASSWORD",unique = true)
    private String password;

    /**
     * set of roles
     */
    @NotNull
    @ManyToMany
    @JoinTable(name = "USER_ROLE", joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Set<Role> roles = new HashSet<>();

    /**
     * Default Constructor
     */
    public User() {
    }

    /**
     *  Constructor without id and Roles
     * @param companyName
     * @param userName
     * @param email
     * @param password
     */
    public User(@NotNull String companyName,
                   @NotNull String userName,
                   @NotNull String email,
                   @NotNull String password) {
        this.companyName = companyName;
        this.userName = userName;
        this.email = email;
        this.password = password;
    }


    /**
     * Gets Unique id.
     *
     * @return Value of Unique id.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Sets new set of roles.
     *
     * @param roles New value of set of roles.
     */
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * Sets new Unique id.
     *
     * @param userId New value of Unique id.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Sets new user firstName.
     *
     * @param companyName New value of user firstName.
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Sets new user password.
     *
     * @param password New value of user password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets set of roles.
     *
     * @return Value of set of roles.
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * Gets user email.
     *
     * @return Value of user email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Gets user companyName.
     *
     * @return Value of user firstName.
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Gets the user name.
     *
     * @return Value of the user name.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets new the user name.
     *
     * @param userName New value of the user name.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Sets new user email.
     *
     * @param email New value of user email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets user password.
     *
     * @return Value of user password.
     */
    public String getPassword() {
        return password;
    }
}