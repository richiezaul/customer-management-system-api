package com.customer_management_system.app.entity;


import com.customer_management_system.app.model.enums.CustomerStatus;
import com.customer_management_system.app.util.EnumValidator;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.boot.SpringApplication;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Customer Entity mapping.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Entity
@Table(name = "CUSTOMERS")
public class Customer {

    /**
     *  the unique customer id
     */
    @Id
    @Column (name = "CUSTOMER_ID", unique = true)
    @GeneratedValue (generator = "system-uuid")
    @GenericGenerator (name = "system-uuid", strategy = "uuid")
    private String customerId;

    /**
     * the first name of the customer
     */
    @Column(name = "FIRST_NAME")
    private String firstName;

    /**
     * the last name of the customer
     */
    @Column(name = "LAST_NAME")
    private String lastName;

    /**
     * customer Social security
     */
    @Column(name = "SSN",unique = true,nullable = false)
    private String socialSecurity;

    /**
     * the timestamp at which the customer is saved in the database
     */
    @Column(name = "CUSTOMER_ENTERED_DATE")
    private ZonedDateTime customerEnteredDate;

    /**
     * the status of the customer
     */
    @Column(name = "CUSTOMER_STATUS")
    @EnumValidator(enumClazz = CustomerStatus.class)
    private String customerStatus;


    /**
     * customers address
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "CUSTOMER_ID")
    private List<Address> customerAddress;





    /**
     * Gets the status of the customer.
     *
     * @return Value of the status of the customer.
     */
    public String getCustomerStatus() {
        return customerStatus;
    }

    /**
     * Sets new the last name of the customer.
     *
     * @param lastName New value of the last name of the customer.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the first name of the customer.
     *
     * @return Value of the first name of the customer.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets the last name of the customer.
     *
     * @return Value of the last name of the customer.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets new the first name of the customer.
     *
     * @param firstName New value of the first name of the customer.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets customers address.
     *
     * @return Value of customers address.
     */
    public List<Address> getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Gets the unique customer id.
     *
     * @return Value of the unique customer id.
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets new the unique customer id.
     *
     * @param customerId New value of the unique customer id.
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * Sets new the status of the customer.
     *
     * @param customerStatus New value of the status of the customer.
     */
    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    /**
     * Sets new the timestamp at which the customer is saved in the database.
     *
     * @param customerEnteredDate New value of the timestamp at which the customer is saved in the database.
     */
    public void setCustomerEnteredDate(ZonedDateTime customerEnteredDate) {
        this.customerEnteredDate = customerEnteredDate;
    }

    /**
     * Sets new customers address.
     *
     * @param customerAddress New value of customers address.
     */
    public void setCustomerAddress(List<Address> customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * Gets the timestamp at which the customer is saved in the database.
     *
     * @return Value of the timestamp at which the customer is saved in the database.
     */
    public ZonedDateTime getCustomerEnteredDate() {
        return customerEnteredDate;
    }


    /**
     * Sets new customer Social security.
     *
     * @param socialSecurity New value of customer Social security.
     */
    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    /**
     * Gets customer Social security.
     *
     * @return Value of customer Social security.
     */
    public String getSocialSecurity() {
        return socialSecurity;
    }
}
