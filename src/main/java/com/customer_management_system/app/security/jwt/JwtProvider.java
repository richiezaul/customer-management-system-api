package com.customer_management_system.app.security.jwt;

import com.customer_management_system.app.entity.MainUser;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 *  JwtEntryPoint
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Component
public class JwtProvider {

    /**
     * Constants logger
     */
    private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    /**
     * secret
     */
    @Value("${jwt.secret}")
    private String secret;

    /**
     * expiration
     */
    @Value("${jwt.expiration}")
    private int expiration;

    /**
     * generateToken
     * @param authentication
     * @return
     */
    public String generateToken(Authentication authentication){
        MainUser usuarioMain = (MainUser) authentication.getPrincipal();
        return Jwts.builder().setSubject(usuarioMain.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + expiration * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * getNombreUsuarioFromToken
     * @param token
     * @return
     */
    public String getUserNameFromToken(String token){

        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * validateToken
     * @param token
     * @return
     */
    public Boolean validateToken(String token){
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        }catch (MalformedJwtException e){
            logger.error("Malformed Token");
        }catch (UnsupportedJwtException e){
            logger.error("Unsupported Token ");
        }catch (ExpiredJwtException e){
            logger.error("Expired Token");
        }catch (IllegalArgumentException e){
            logger.error("Empty Token");
        }catch (SignatureException e){
            logger.error("fail Signature");
        }
        return false;
    }
}