package com.customer_management_system.app.controller;


import com.customer_management_system.app.model.CustomerRequest;
import com.customer_management_system.app.model.dto.CustomerDto;
import com.customer_management_system.app.model.dto.ServiceResponseDto;
import com.customer_management_system.app.model.enums.CustomerStatus;
import com.customer_management_system.app.service.CustomersService;
import com.customer_management_system.app.util.Constants;
import com.customer_management_system.app.util.EnumValidator;
import com.customer_management_system.app.util.Util;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Customer  Controller Class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
@Validated
@RestController
@RequestMapping(Constants.PATH)
public class CustomerController {

    /**
     * The constant logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);//LOGGER

    /**
     * The customer code service
     */
    private final CustomersService customersService;

    /**
     * mapper
     */
    ModelMapper mapper= new ModelMapper();


    /***
     * bean Injection by Constructor
     * @param itemService
     */
    @Autowired
    public CustomerController(CustomersService itemService) {
        this.customersService = itemService;
    }


    /**
     * getCustomers controller method
     *
     * @return a {@code ResponseEntity<ServiceResponseDto>}
     */
    @GetMapping(value = Constants.ENDPOINT_CUSTOMERS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResponseDto> getCustomers(@RequestParam(required = false) String ssn)  {

        List<CustomerDto> customerDtoList;

        ServiceResponseDto response = new ServiceResponseDto();

        if (Objects.isNull(ssn)){

            customerDtoList = customersService.getAllCustomers().get();
            response.setData(customerDtoList);
        }else {

            final Optional<CustomerDto> customerBySsn = customersService.getCustomerBySsn(ssn);
            response.setData(Collections.singletonList(customerBySsn.get()));

        }

        LOGGER.info(Constants.URL_CALL_MESSAGE, Constants.ENDPOINT_CUSTOMERS);

        return ResponseEntity.status(HttpStatus.OK).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(response);
    }


    /**
     * deleteCustomers controller method
     *
     * @return a {@code ResponseEntity<ServiceResponseDto>}
     */
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = Constants.ENDPOINT_CUSTOMERS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResponseDto> deleteCustomers(@RequestParam(required = true) String ssn)  {

        LOGGER.info(Constants.URL_CALL_MESSAGE, Constants.ENDPOINT_CUSTOMERS);

        customersService.deleteCustomerBySsn(ssn);

        return ResponseEntity.status(HttpStatus.OK).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(new ServiceResponseDto());

    }


    /**
     * getCustomerPageAndSortBy controller method
     *
     * @return a {@code ResponseEntity<ServiceResponseDto>}
     */
    @GetMapping(value = Constants.ENDPOINT_ITEMS_PAGE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResponseDto> getCustomerPageAndSortBy(
             @RequestParam(required = true) @Min(value = 1) @NotNull Integer pageSize,
             @RequestParam(required = true) @Min(value = 1) @NotNull Integer page,
             @RequestParam(required = true) @NotBlank String sortBy)  {

        LOGGER.info(Constants.URL_CALL_MESSAGE, Constants.ENDPOINT_ITEMS_PAGE);
        Util.validSortParam(sortBy, CustomerDto.class);

        Integer localPage= page-1;
        ServiceResponseDto response = new ServiceResponseDto();

        List<CustomerDto> data = customersService.getCustomerPageAndSortByField(PageRequest.of(localPage,pageSize, Sort.by(sortBy))).get();
        response.setData(data);

        return ResponseEntity.status(HttpStatus.OK).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(response);

    }


    /**
     * saveCustomer controller method
     *
     * @return a {@code ResponseEntity<ServiceResponseDto>}
     */
    @PostMapping(value = Constants.ENDPOINT_CUSTOMERS, produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResponseDto> saveCustomer(@RequestBody @Valid @NotNull CustomerRequest customerRequest) throws Exception {

        LOGGER.info(Constants.URL_CALL_MESSAGE, Constants.ENDPOINT_CUSTOMERS);
        ServiceResponseDto response = new ServiceResponseDto();

        final CustomerDto customerDto = this.mapper.map(customerRequest, CustomerDto.class);

        final CustomerDto data = customersService.addCustomers(customerDto).get();
        response.setData(Collections.singletonList(data));

        return ResponseEntity.status(HttpStatus.CREATED).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(response);

    }


    /**
     * updateCustomers controller method
     *
     * @return a {@code ResponseEntity<ServiceResponseDto>}
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = Constants.ENDPOINT_CUSTOMERS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResponseDto> updateCustomers(@RequestParam String ssn, @RequestParam
    @EnumValidator(enumClazz = CustomerStatus.class) String status ) throws Exception {

        LOGGER.info(Constants.URL_CALL_MESSAGE, Constants.ENDPOINT_CUSTOMERS);
        ServiceResponseDto response = new ServiceResponseDto();

        final CustomerDto data = customersService.updateCustomers(status,ssn).get();
        response.setData(Collections.singletonList(data));

        return ResponseEntity.status(HttpStatus.CREATED).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(response);
    }


}
